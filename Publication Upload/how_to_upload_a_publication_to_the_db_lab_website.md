# How to upload a publication to the db lab website


* firstly, click on [this link](http://db.ucsd.edu/wp-admin) to visit the db lab backend (http://db.ucsd.edu/wp-admin). A page like the following should open up

![image 1](https://gitlab.com/czarifis/DBLabnotes/raw/master/Publication%20Upload/img1.png)



* Use the credentials that were given to you to log in. After successfully logging in, this page should appear:

 ![dashboard](https://gitlab.com/czarifis/DBLabnotes/raw/master/Publication%20Upload/img2.png)

* On the left hand side menu, there will be a "Publications" option, click on it to see a list with all the publications:

![publications](https://gitlab.com/czarifis/DBLabnotes/raw/master/Publication%20Upload/img3.png)

* Click on "add new"
* On the page that opens up you should fill in the following fields:
	* **The title** (it is the very first field)	
	* **The type of the publications** (use the drop down menu, e.g Journal Article, Conference, etc)
	* **The authors.**
        Each author must be separated from the other authors using the keyword "and". If for example the authors of a particular paper are Yannis Katsis, Kian Win Ong and Yannis Papakonstantinou the authors' field for this particular entry would be: "*Yannis Katsis and Kian Win Ong and Yannis Papakonstantinou*".

        Since typos might occur there, the author list supports auto completion, so if you start typing a name that has already been imported before it should appear bellow the field, if you click on a name it gets added to the field and the word "and" appears next to it. 

        Since the papers are queried using the author name make sure that there is only one entry per person. If you are inserting a new name for the first time make sure that you type it correctly. If you spot a typo to an author name, go to the "*Authors*" menu, delete the entry and add the name of the author to a paper, after doing so it will be added automatically to the Authors list. The image bellow shows how the autocompletion works, by typing "yan".
    * **The date of publishing** This is the date in which the paper was published. Papers are sorted by the publishing date so make sure it is precise.
    * **Details about the publication** Bellow the date of publishing there is another field that let's you add details about the current publication. If you want to add the conference name, the Journal name and the pages in which the publication appears, this is the right field to add that information to. The title of that field depends on the type of publication you chose on the second step. For example, in the picture bellow the current publication is a Journal Article (it appears on the drop down) therefore the field that contains the details of the publication has the title "journal".
    * **Attach file**. If you want to include a downloadable pdf file with the publication, scroll down until you find the field: "URL/Files" press "Add/Upload", then click on "Choose File", select the pdf file from your local disk, click "Upload" and on the next window click "Insert into Post".
    * **BibTex Key** All the publications require a BibTex key, so scroll up until you find the "BibTeX Key" title and click on the refresh icon.
    * **Tags**. All the publications also require a collection of tags, so on the right hand side add some keywords that are associated with the current publication. Seperate the keywords with commas.

DONE! After completing everything click on "Create" and visit the publications page (http://db.ucsd.edu/?page_id=758) to make sure that everything appears correctly. 

if adjustments have to be made, visit the admin page again, click on "Publications" locate the publication that requires the changes and modify it appropriately. Repeat the steps until you get the expected result.
	
![publications](https://gitlab.com/czarifis/DBLabnotes/raw/master/Publication%20Upload/img4.png)